package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class ResultadosActivity extends AppCompatActivity {
    private ListView lstResultados;
    private Button btnLimpiar;
    private Button btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);
        lstResultados = (ListView)findViewById(R.id.lstResultados);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);
        Bundle datos = getIntent().getExtras();
        String tabla = datos.getString("tabla");
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(ResultadosActivity.this, android.R.layout.simple_expandable_list_item_1,crearTabla(tabla));
        lstResultados.setAdapter(adapter);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public ArrayList<String> crearTabla(String tabla){
        ArrayList<String>  resultados = new ArrayList<>();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(getResources().getStringArray(R.array.Tablas)));
        for (int x=0; x <= arrayList.size(); x++){
            if(tabla.matches(arrayList.get(x))){
                for (int y=0; y<=10; y++){
                    int res = (x+1) * y;
                    String ta = (x+1) + "x" + y + "=" + res;
                    resultados.add(ta);
                }
                break;
            }
        }
        return resultados;
    }
}
